﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectUniFinal.Entities;
using Microsoft.EntityFrameworkCore;

namespace ProjectUniFinal.Models
{
    public class Datacontext : DbContext
    {
        public Datacontext (DbContextOptions<Datacontext> options) : base (options)
        {

        }
        public DbSet<ProductsClass> Product { get; set; }
    }
}
