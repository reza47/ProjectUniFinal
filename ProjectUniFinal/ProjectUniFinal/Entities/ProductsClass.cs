﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace ProjectUniFinal.Entities
{
    public class ProductsClass
    {
        [Key]
        public int Product_Id { get; set; }
        public string Product_Name { get; set; }
        public string Product_Number { get; set; }
        public int Serial_Product { get; set; }
        public string Price { get; set; }
    }
}
