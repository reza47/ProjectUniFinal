﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectUniFinal.Models;
using ProjectUniFinal.Entities;

namespace ProjectUniFinal.Controllers
{
    public class HomeController : Controller
    {
        private readonly Datacontext db;
        public HomeController(Datacontext _db)
        {
            db = _db;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost] 
        //public IActionResult AddProduct(ProductsClass products)
        //{
        //    db.Product.Add();
        //}

        public IActionResult AnbarDari ()
        {
            ProductsClass products = new ProductsClass();
            return View(products);
            
           
        }

        public IActionResult Seller()
        {
            ViewData["Message"] = "به صفحه پنل فروشنده خوش آمدید";

            return View();
        }

        public IActionResult Customer()
        {
            ViewData["Message"] = "به صفحه پنل خریدار خوش آمدید.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
